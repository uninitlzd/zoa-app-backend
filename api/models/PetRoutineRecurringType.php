<?php
/**
 * Created by PhpStorm.
 * User: alfredo
 * Date: 11/22/2017
 * Time: 10:07 AM
 */

namespace api\models;


use yii\db\ActiveRecord;

class PetRoutineRecurringType extends ActiveRecord
{
    public static function tableName()
    {
        return 'pet_routine_recurring_type';
    }
}