<?php
/**
 * Created by PhpStorm.
 * User: alfredo
 * Date: 11/24/2017
 * Time: 3:54 PM
 */

namespace api\models;


use http\Url;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Exception;

class AccountVerificationToken extends ActiveRecord
{
    public static function tableName()
    {
        return 'user_verification_token';
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @param $token
     * @param $type
     * $type = 1 for email registration
     * $type = 2 for password reset
     * @param $email
     * @return bool
     */
    public static function sendVerification($token = null, $type, $email)
    {

        switch ($type) {
            case 1:
                $verificationUrl = "http://api.zoa.id" . \yii\helpers\Url::to(['account/verify', 'token' => $token, 'type' => $type]);
                Yii::$app->mailer->compose()
                    ->setFrom('zoa@outlook.co.id')
                    ->setTo($email)
                    ->setSubject('Zoa Verification')
                    ->setHtmlBody('<b>This is your verification link: </b> ' . "<a>$verificationUrl</a>" )
                    ->send();

                return ["verification sent"];
                break;
            case 2:

                break;
        }
    }
}