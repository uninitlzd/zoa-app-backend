<?php
/**
 * Created by PhpStorm.
 * User: alfredo
 * Date: 11/20/2017
 * Time: 10:29 AM
 */

namespace api\models;


use yii\db\ActiveRecord;

class PetFamily extends ActiveRecord
{
    public static function tableName()
    {
        return 'pet_families';
    }
}