<?php
/**
 * Created by PhpStorm.
 * User: alfredo
 * Date: 11/20/2017
 * Time: 10:18 AM
 */

namespace api\models;

use yii\db\ActiveRecord;

class Pet extends ActiveRecord
{
    public static function tableName()
    {
        return 'pets';
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getDetail()
    {
        return $this->hasOne(PetDetail::className(), ['pet_id' => 'id']);
    }

}