<?php
/**
 * Created by PhpStorm.
 * User: alfredo
 * Date: 11/24/2017
 * Time: 2:11 PM
 */

namespace api\controllers;


use api\models\AccountVerificationToken;
use api\models\User;
use Yii;
use yii\helpers\Url;

class TestController extends BaseController
{
    public function actionMail()
    {
        var_dump(Yii::$app->mailer->compose()
            ->setFrom('zoa@outlook.co.id')
            ->setTo('alfredoeka@outlook.com')
            ->setSubject('Message subject')
            ->setTextBody('Plain text content')
            ->setHtmlBody('<b>HTML content</b>')
            ->send());

        return "oke";
    }

    public function actionVerification()
    {
        $user = User::findOne(3);
        return $user->setVerificationToken(1);
        $verificationUrl = "http://api.zoa.id" . Url::to(['account/verify', 'token' => $user->verificationToken->token, 'type' => 1]);
        return $verificationUrl;
    }
}