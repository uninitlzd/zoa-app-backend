<?php

namespace api\controllers;

use api\transformers\UserProfileTransformer;
use api\transformers\UserTransformer;
use Yii;
use api\models\User;
use yii\rest\ActiveController;
use yii\filters\ContentNegotiator;
use yii\web\Response;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBearerAuth;
use League\Fractal;
use League\Fractal\Manager;

class UserController extends BaseController
{
    public $modelClass = 'api\models\User';
    public $enableCsrfValidation = false;
    
    public function behaviors()
    {
        return [
             'authMethods' => [
                 'class' => CompositeAuth::className(),
                 'authMethods' => [
                     HttpBearerAuth::className(),
                 ]
             ],
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    public function actionProfile()
    {
        return $this->item(Yii::$app->user->identity, new UserProfileTransformer(), 'user_profile');
    }
   
}