<?php
namespace api\controllers;

use League\Fractal\Resource\Collection;
use Yii;
use yii\web\Controller;
use yii\filters\ContentNegotiator;
use yii\web\Response;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Serializer\DataArraySerializer;

class BaseController extends Controller
{
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                // if in a module, use the following IDs for user actions
                // 'only' => ['user/view', 'user/index']
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    public function item($model, $transformer, $name)
    {
        $manager = new Manager();
        if (isset($_GET['include'])) {
            $manager->parseIncludes($_GET['include']);
        }
        $manager->setSerializer(new DataArraySerializer());

        // Make a resource out of the data and
        $resource = new Item($model, $transformer, $name);

        // Run all transformers
        return $manager->createData($resource)->toArray();
    }

    public function collection($model, $transformer, $name)
    {
        $manager = new Manager();
        if (isset($_GET['include'])) {
            $manager->parseIncludes($_GET['include']);
        }
        $manager->setSerializer(new DataArraySerializer());

        // Make a resource out of the data and
        $resource = new Collection($model, $transformer, $name);

        // Run all transformers
        return $manager->createData($resource)->toArray();
    }

    public static function getAuthenticatedUser()
    {
        return Yii::$app->user->identity;
    }
}
    