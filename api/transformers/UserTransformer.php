<?php
namespace api\transformers;
use League\Fractal;
use api\models\User;

class UserTransformer extends Fractal\TransformerAbstract {

    protected $availableIncludes = [
        'pet'
    ];

    public function transform(User $user)
	{
	    return [
			'id'            => (int) $user->id,
            'first_name'    => $user->first_name,
            'last_name'     => $user->last_name,
			'username'      => $user->username,
			'email'         => $user->email,
            'picture'       => $user->picture,
			'access_token'  => $user->access_token,
            'is_verified'   => $user->is_verified
	    ];
	}

	public function includePet(User $user)
    {
        $pet = $user->pet;
        return $this->collection($pet, new PetTransformer());
    }
}