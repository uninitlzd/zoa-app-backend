<?php
/**
 * Created by PhpStorm.
 * User: alfredo
 * Date: 11/22/2017
 * Time: 12:17 AM
 */

namespace api\transformers;


use api\models\User;
use League\Fractal\TransformerAbstract;

class UserProfileTransformer extends TransformerAbstract
{
    public function transform(User $user)
    {
        return [
            'id'            => (int) $user->id,
            'first_name'    => $user->first_name,
            'last_name'     => $user->last_name,
            'username'      => $user->username,
            'email'         => $user->email,
            'picture'       => $user->picture
        ];
    }
}