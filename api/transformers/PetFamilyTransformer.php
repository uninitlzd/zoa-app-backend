<?php
/**
 * Created by PhpStorm.
 * User: alfredo
 * Date: 11/21/2017
 * Time: 10:29 PM
 */

namespace api\transformers;


use api\models\PetFamily;
use League\Fractal\TransformerAbstract;

class PetFamilyTransformer extends TransformerAbstract
{
    public function transform(PetFamily $family)
    {
        return [
            'id'            => (int) $family->id,
            'pet_class_id'  => $family->pet_class_id,
            'name'          => $family->name
        ];
    }
}