<?php

use yii\db\Migration;

/**
 * Handles adding is_verified to table `user`.
 */
class m171124_084725_add_is_verified_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('users', 'is_verified', $this->boolean()->defaultValue(false));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('users', 'is_verified');
    }
}
