<?php

use yii\db\Migration;

class m171120_024033_create_table_pets extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('pets', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'name' => $this->string(50)->notNull(),
            'gender' => $this->char(1),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp()
        ]);
    }

    public function down()
    {
        $this->dropTable('pets');
    }

}
