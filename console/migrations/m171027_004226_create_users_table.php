<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m171027_004226_create_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
       $this->createTable('users', [
            'id' => $this->primaryKey(),
            'first_name' => $this->string()->notNull(),
            'last_name' => $this->string(),
            'username' => $this->string()->unique(),
            'authKey' => $this->string()->notNull(),
            'password' => $this->string()->notNull(),
            'email' => $this->string()->notNull()->unique(),
            'access_token' => $this->text()->notNull(),
            'device_token' => $this->text(),
            'picture' => $this->text(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('users');
    }
}
