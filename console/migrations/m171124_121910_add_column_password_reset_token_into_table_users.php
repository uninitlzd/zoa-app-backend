<?php

use yii\db\Migration;

class m171124_121910_add_column_password_reset_token_into_table_users extends Migration
{
    /*public function safeUp()
    {

    }

    public function safeDown()
    {
        echo "m171124_121910_add_column_password_reset_token_into_table_users cannot be reverted.\n";

        return false;
    }*/

    public function up()
    {
        $this->addColumn('users', 'password_reset_token', $this->string()->null());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('users', 'password_reset_token');
    }
}
