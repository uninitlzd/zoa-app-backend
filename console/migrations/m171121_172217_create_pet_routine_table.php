<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pet_routine`.
 */
class m171121_172217_create_pet_routine_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('pet_routines', [
            'id' => $this->primaryKey(),
            'pet_id' => $this->integer(),
            'event_title' => $this->string(),
            'event_description' => $this->text(),
            'start_date' => $this->date(),
            'end_date' => $this->date(),
            'start_time' => $this->timestamp(),
            'end_time' => $this->timestamp(),
            'is_recurring' => $this->boolean(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('pet_routines');
    }
}
