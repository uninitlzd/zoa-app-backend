<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pet_races`.
 */
class m171120_025158_create_pet_races_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('pet_races', [
            'id' => $this->primaryKey(),
            'pet_family_id' => $this->integer(),
            'name' => $this->string(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('pet_races');
    }
}
