<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pet_families`.
 */
class m171120_025145_create_pet_families_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('pet_families', [
            'id' => $this->primaryKey(),
            'pet_class_id' => $this->integer(),
            'name' => $this->string(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp()

        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('pet_families');
    }
}
